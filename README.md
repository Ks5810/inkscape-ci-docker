## CI Base for Inkscape

Trying to make the CI builds for Inkscape run faster with a Docker overlay.

For each CI build we were using a base OS image, and then installing all of the dependencies that we needed on it. By using a Docker overlay we can install those dependencies once, and then overlay it for each build. This speeds up our CI builds (as there can be several in a pipeline) and reduces the amount of text in the logs.

At the end of the day, our images end up in the [container registry of the inkscape-ci-docker project](https://gitlab.com/inkscape/inkscape-ci-docker/container_registry). You can use them with this:

```bash
docker pull registry.gitlab.com/inkscape/inkscape-ci-docker/master
```

The images should be rebuilt weekly or when someone changes the Dockerfile in this repo.

